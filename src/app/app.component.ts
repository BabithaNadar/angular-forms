import { Component, Inject, EventEmitter, Output } from '@angular/core';
import { EmpList } from './models/emp.list';
import { Emp } from './models/emp';
import { EmpObjList } from './models/emp.obj.list';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { EmpObj } from './models/emp.obj';
import { Observable } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { EmpService } from './services/emp.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [EmpService]
})
export class AppComponent {

  empDetails: Array<Emp>;
  empList: Array<EmpObj>;
  empForm: FormGroup;
  sort_item: string;
  ascId: boolean = false;
  ascName: boolean = true;
  ascSal: boolean = true; 
  ascAge: boolean = true;
  searchByName: string;
  searchChangeObserver;
  showDepartment:boolean = false;
  depts = [];
  //questions$: Observable<QuestionBase<any>[]>;

  constructor(private empService: EmpService, private fb: FormBuilder) {
    this.buildForm();
    this.empDetails = [];
    this.empList = [];
    //this.questions$ = this.service.getQuestions();
    this.depts = [
      'Developer', 'Tester', 'Management', 'Others'
    ]
  }

  ngOnInit() {
    this.empService.fetchAllEmp().subscribe((empList: EmpList) => {
      this.empDetails = empList.data;
    })
  }

  buildForm() {
    this.empForm = this.fb.group({
      id: [null, [Validators.required, Validators.pattern('[0-9]+')]],
      name: [null, [Validators.required]],
      age: [null, [Validators.required, Validators.max(150), Validators.pattern('[0-9]+')]],
      department: ['']
    });
  }

  get id(): FormControl {
    return this.empForm.get('id') as FormControl;
  }
  get name(): FormControl {
    return this.empForm.get('name') as FormControl;
  }
  get age(): FormControl {
    return this.empForm.get('age') as FormControl;
  }
  get department(): FormControl {
    return this.empForm.get('department') as FormControl;
  }

  changeCity(e) {
    this.department.setValue(e.target.value, {
      onlySelf: true
    })
  }

  submitForm() {
    this.empService.createEmp(this.empForm.value as EmpObj).subscribe((emp: EmpObjList) => {
      if (emp.data !== undefined) {
        this.empForm.value.id = emp.data.id;
        this.empList.push(this.empForm.value);
        alert(`Employee added successfully!`);
      }
      else {
        alert(`Employee not added...`);
      }
    })
  }

  sort(sortby: string) {
    this.sort_item = sortby;
    switch (this.sort_item) {
      case 'id':
        if (this.ascId) {
          this.empDetails.sort((a, b) => {
            return a.id - b.id;
          });
        }
        else {
          this.empDetails.sort((a, b) => {
            return a.id - b.id;
          }).reverse();
        }
        this.ascId = !this.ascId;
        break;
      case 'employee_age':
        if (this.ascAge) {
          this.empDetails.sort((a, b) => a.employee_age.localeCompare(b.employee_age));
        }
        else {
          this.empDetails.sort((a, b) => a.employee_age.localeCompare(b.employee_age)).reverse();
        }
        this.ascAge = !this.ascAge;
        break;
      case 'employee_salary':
        if (this.ascSal) {
          this.empDetails.sort((a, b) => {
            return a.employee_salary - b.employee_salary;
          })
        }
        else {
          this.empDetails.sort((a, b) => {
            return a.employee_salary - b.employee_salary;
          }).reverse();
        }
        this.ascSal = !this.ascSal;
        break;
      case 'employee_name':
        if (this.ascName) {
          this.empDetails.sort((a, b) => a.employee_name.localeCompare(b.employee_name));
        }
        else {
          this.empDetails.sort((a, b) => a.employee_name.localeCompare(b.employee_name)).reverse();
        }
        this.ascName = !this.ascName;
        break;
      default: console.log('Unable to sort');
    }
  }

  getEmpByName(value: string) {
    if (!this.searchChangeObserver) {
      Observable.create((observer: any) => {
        this.searchChangeObserver = observer;
      }).pipe(debounceTime(1000), distinctUntilChanged()) // wait 300ms after the last event before emitting last event// only emit if value is different from previous value
        .subscribe((search:string) => {
          this.searchByName = search;
          this.empDetails = [];
          this.empService.fetchAllEmp().subscribe((empList: EmpList) => {
            empList.data.forEach((data) => {
              if (data.employee_name.toUpperCase().startsWith( this.searchByName.toUpperCase())) {
                this.empDetails.push(data);
              }
            });
          })
        });
    }
    this.searchChangeObserver.next(value);
  }
}