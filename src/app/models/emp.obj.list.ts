import { EmpObj } from './emp.obj';

export interface EmpObjList {
    status: string,
    data:EmpObj
}