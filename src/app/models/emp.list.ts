import { Emp } from './emp';

export interface EmpList {
    status: string,
    data: Emp[]
}