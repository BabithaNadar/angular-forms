export interface EmpObj {
    id: string,
    name: string,
    salary?:string,
    age: string,
    department?: string
}