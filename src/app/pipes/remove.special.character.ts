import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'removespecialcharacter'
})
export class RemoveSpecialCharacter implements PipeTransform {

    transform(value: string,args?: any): string{
        return value.replace(/[0-9.*+,#!&?^@%${}()|[\]\\]/g, '');
    }
}