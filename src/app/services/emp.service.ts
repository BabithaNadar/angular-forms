import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EmpList } from '../models/emp.list';
import { Observable } from 'rxjs';
import { URLS } from '../constants/url.constants';
import { Emp } from '../models/emp';
import { EmpObjList } from '../models/emp.obj.list';
import { EmpObj } from '../models/emp.obj';

@Injectable()
export class EmpService {
    constructor(private httpClient: HttpClient) { }

    fetchAllEmp(): Observable<EmpList> {
        return this.httpClient.get<EmpList>(URLS.getEmpUrl);
    }

    createEmp(emp: EmpObj): Observable<EmpObjList> {
        return this.httpClient.post<EmpObjList>(URLS.createEmpUrl, JSON.stringify(emp), {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    }
}